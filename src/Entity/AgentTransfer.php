<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AgentTransfer
 *
 * @ORM\Table(name="agent_transfer")
 * @ORM\Entity
 */
class AgentTransfer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="agentId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $agentid;

    /**
     * @var int
     *
     * @ORM\Column(name="fromAgentId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $fromagentid;

    /**
     * @var int
     *
     * @ORM\Column(name="toAgentId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $toagentid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    /**
     * @var int|null
     *
     * @ORM\Column(name="completed", type="smallint", nullable=true)
     */
    private $completed;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgentid(): ?int
    {
        return $this->agentid;
    }

    public function setAgentid(int $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }

    public function getFromagentid(): ?int
    {
        return $this->fromagentid;
    }

    public function setFromagentid(int $fromagentid): self
    {
        $this->fromagentid = $fromagentid;

        return $this;
    }

    public function getToagentid(): ?int
    {
        return $this->toagentid;
    }

    public function setToagentid(int $toagentid): self
    {
        $this->toagentid = $toagentid;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getCompleted(): ?int
    {
        return $this->completed;
    }

    public function setCompleted(?int $completed): self
    {
        $this->completed = $completed;

        return $this;
    }


}

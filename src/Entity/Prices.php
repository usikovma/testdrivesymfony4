<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prices
 *
 * @ORM\Table(name="prices", uniqueConstraints={@ORM\UniqueConstraint(name="prices_agentId_requestId_idx", columns={"agentId", "requestId"})}, indexes={@ORM\Index(name="IDX_E4CB6D59A1637001", columns={"requestId"}), @ORM\Index(name="IDX_E4CB6D5917EB4E41", columns={"agentId"})})
 * @ORM\Entity
 */
class Prices
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="isOwner", type="smallint", nullable=false)
     */
    private $isowner;

    /**
     * @var int|null
     *
     * @ORM\Column(name="invoiceId", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $invoiceid = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=false)
     */
    private $tstamp;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentId", referencedColumnName="id")
     * })
     */
    private $agentid;

    /**
     * @var \Requests
     *
     * @ORM\ManyToOne(targetEntity="Requests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requestId", referencedColumnName="id")
     * })
     */
    private $requestid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsowner(): ?int
    {
        return $this->isowner;
    }

    public function setIsowner(int $isowner): self
    {
        $this->isowner = $isowner;

        return $this;
    }

    public function getInvoiceid(): ?int
    {
        return $this->invoiceid;
    }

    public function setInvoiceid(?int $invoiceid): self
    {
        $this->invoiceid = $invoiceid;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getAgentid(): ?Agents
    {
        return $this->agentid;
    }

    public function setAgentid(?Agents $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }

    public function getRequestid(): ?Requests
    {
        return $this->requestid;
    }

    public function setRequestid(?Requests $requestid): self
    {
        $this->requestid = $requestid;

        return $this;
    }


}

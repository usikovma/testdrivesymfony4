<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarCheck
 *
 * @ORM\Table(name="car_check")
 * @ORM\Entity
 */
class CarCheck
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="identTypeId", type="smallint", nullable=false)
     */
    private $identtypeid;

    /**
     * @var string
     *
     * @ORM\Column(name="ident", type="string", length=255, nullable=false)
     */
    private $ident;

    /**
     * @var string|null
     *
     * @ORM\Column(name="power", type="string", length=255, nullable=true)
     */
    private $power;

    /**
     * @var string|null
     *
     * @ORM\Column(name="markmodel", type="string", length=255, nullable=true)
     */
    private $markmodel;

    /**
     * @var string|null
     *
     * @ORM\Column(name="year", type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mass", type="string", length=255, nullable=true)
     */
    private $mass;

    /**
     * @var string|null
     *
     * @ORM\Column(name="maxMass", type="string", length=255, nullable=true)
     */
    private $maxmass;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdenttypeid(): ?int
    {
        return $this->identtypeid;
    }

    public function setIdenttypeid(int $identtypeid): self
    {
        $this->identtypeid = $identtypeid;

        return $this;
    }

    public function getIdent(): ?string
    {
        return $this->ident;
    }

    public function setIdent(string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    public function getPower(): ?string
    {
        return $this->power;
    }

    public function setPower(?string $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getMarkmodel(): ?string
    {
        return $this->markmodel;
    }

    public function setMarkmodel(?string $markmodel): self
    {
        $this->markmodel = $markmodel;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getMass(): ?string
    {
        return $this->mass;
    }

    public function setMass(?string $mass): self
    {
        $this->mass = $mass;

        return $this;
    }

    public function getMaxmass(): ?string
    {
        return $this->maxmass;
    }

    public function setMaxmass(?string $maxmass): self
    {
        $this->maxmass = $maxmass;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }


}

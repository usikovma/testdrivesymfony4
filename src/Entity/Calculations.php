<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calculations
 *
 * @ORM\Table(name="calculations", indexes={@ORM\Index(name="IDX_4BFD195EA1637001", columns={"requestId"})})
 * @ORM\Entity
 */
class Calculations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="productId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $productid;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="insuranceIdentifier", type="string", length=255, nullable=true)
     */
    private $insuranceidentifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="additionalData", type="text", length=0, nullable=true)
     */
    private $additionaldata;

    /**
     * @var int|null
     *
     * @ORM\Column(name="completed", type="smallint", nullable=true)
     */
    private $completed;

    /**
     * @var \Requests
     *
     * @ORM\ManyToOne(targetEntity="Requests")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="requestId", referencedColumnName="id")
     * })
     */
    private $requestid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function setProductid(int $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getInsuranceidentifier(): ?string
    {
        return $this->insuranceidentifier;
    }

    public function setInsuranceidentifier(?string $insuranceidentifier): self
    {
        $this->insuranceidentifier = $insuranceidentifier;

        return $this;
    }

    public function getAdditionaldata(): ?string
    {
        return $this->additionaldata;
    }

    public function setAdditionaldata(?string $additionaldata): self
    {
        $this->additionaldata = $additionaldata;

        return $this;
    }

    public function getCompleted(): ?int
    {
        return $this->completed;
    }

    public function setCompleted(?int $completed): self
    {
        $this->completed = $completed;

        return $this;
    }

    public function getRequestid(): ?Requests
    {
        return $this->requestid;
    }

    public function setRequestid(?Requests $requestid): self
    {
        $this->requestid = $requestid;

        return $this;
    }


}

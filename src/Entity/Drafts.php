<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Drafts
 *
 * @ORM\Table(name="drafts", indexes={@ORM\Index(name="IDX_EC2AE4C017EB4E41", columns={"agentId"})})
 * @ORM\Entity
 */
class Drafts
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="productTypeId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $producttypeid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="car", type="string", length=255, nullable=true)
     */
    private $car;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ident", type="string", length=255, nullable=true)
     */
    private $ident;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fio", type="string", length=255, nullable=true)
     */
    private $fio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data", type="text", length=0, nullable=true)
     */
    private $data;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $status;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentId", referencedColumnName="id")
     * })
     */
    private $agentid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducttypeid(): ?int
    {
        return $this->producttypeid;
    }

    public function setProducttypeid(int $producttypeid): self
    {
        $this->producttypeid = $producttypeid;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getCar(): ?string
    {
        return $this->car;
    }

    public function setCar(?string $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getIdent(): ?string
    {
        return $this->ident;
    }

    public function setIdent(?string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(?string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAgentid(): ?Agents
    {
        return $this->agentid;
    }

    public function setAgentid(?Agents $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }


}

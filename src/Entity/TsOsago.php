<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TsOsago
 *
 * @ORM\Table(name="ts_osago")
 * @ORM\Entity
 */
class TsOsago
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="inner_model", type="string", length=100, nullable=false)
     */
    private $innerModel;

    /**
     * @var string
     *
     * @ORM\Column(name="inner_mark", type="string", length=100, nullable=false)
     */
    private $innerMark;

    /**
     * @var string
     *
     * @ORM\Column(name="ingos_isn", type="string", length=20, nullable=false)
     */
    private $ingosIsn = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ingos_model", type="string", length=100, nullable=false)
     */
    private $ingosModel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ingos_mark", type="string", length=100, nullable=false)
     */
    private $ingosMark = '';

    /**
     * @var string
     *
     * @ORM\Column(name="alpha_model", type="string", length=100, nullable=false)
     */
    private $alphaModel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="alpha_mark", type="string", length=100, nullable=false)
     */
    private $alphaMark = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sib_model_id", type="string", length=10, nullable=false)
     */
    private $sibModelId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sib_model_name", type="string", length=100, nullable=false)
     */
    private $sibModelName = '';

    /**
     * @var string
     *
     * @ORM\Column(name="sib_mark_id", type="string", length=10, nullable=false)
     */
    private $sibMarkId = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ren_model", type="string", length=100, nullable=false)
     */
    private $renModel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="ren_mark", type="string", length=100, nullable=false)
     */
    private $renMark = '';

    /**
     * @var string
     *
     * @ORM\Column(name="rgs_model", type="string", length=100, nullable=false)
     */
    private $rgsModel = '';

    /**
     * @var string
     *
     * @ORM\Column(name="rgs_mark", type="string", length=100, nullable=false)
     */
    private $rgsMark = '';

    /**
     * @var string
     *
     * @ORM\Column(name="rgs_code", type="string", length=100, nullable=false)
     */
    private $rgsCode = '';

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=false)
     */
    private $type = '';

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=100, nullable=false)
     */
    private $category = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInnerModel(): ?string
    {
        return $this->innerModel;
    }

    public function setInnerModel(string $innerModel): self
    {
        $this->innerModel = $innerModel;

        return $this;
    }

    public function getInnerMark(): ?string
    {
        return $this->innerMark;
    }

    public function setInnerMark(string $innerMark): self
    {
        $this->innerMark = $innerMark;

        return $this;
    }

    public function getIngosIsn(): ?string
    {
        return $this->ingosIsn;
    }

    public function setIngosIsn(string $ingosIsn): self
    {
        $this->ingosIsn = $ingosIsn;

        return $this;
    }

    public function getIngosModel(): ?string
    {
        return $this->ingosModel;
    }

    public function setIngosModel(string $ingosModel): self
    {
        $this->ingosModel = $ingosModel;

        return $this;
    }

    public function getIngosMark(): ?string
    {
        return $this->ingosMark;
    }

    public function setIngosMark(string $ingosMark): self
    {
        $this->ingosMark = $ingosMark;

        return $this;
    }

    public function getAlphaModel(): ?string
    {
        return $this->alphaModel;
    }

    public function setAlphaModel(string $alphaModel): self
    {
        $this->alphaModel = $alphaModel;

        return $this;
    }

    public function getAlphaMark(): ?string
    {
        return $this->alphaMark;
    }

    public function setAlphaMark(string $alphaMark): self
    {
        $this->alphaMark = $alphaMark;

        return $this;
    }

    public function getSibModelId(): ?string
    {
        return $this->sibModelId;
    }

    public function setSibModelId(string $sibModelId): self
    {
        $this->sibModelId = $sibModelId;

        return $this;
    }

    public function getSibModelName(): ?string
    {
        return $this->sibModelName;
    }

    public function setSibModelName(string $sibModelName): self
    {
        $this->sibModelName = $sibModelName;

        return $this;
    }

    public function getSibMarkId(): ?string
    {
        return $this->sibMarkId;
    }

    public function setSibMarkId(string $sibMarkId): self
    {
        $this->sibMarkId = $sibMarkId;

        return $this;
    }

    public function getRenModel(): ?string
    {
        return $this->renModel;
    }

    public function setRenModel(string $renModel): self
    {
        $this->renModel = $renModel;

        return $this;
    }

    public function getRenMark(): ?string
    {
        return $this->renMark;
    }

    public function setRenMark(string $renMark): self
    {
        $this->renMark = $renMark;

        return $this;
    }

    public function getRgsModel(): ?string
    {
        return $this->rgsModel;
    }

    public function setRgsModel(string $rgsModel): self
    {
        $this->rgsModel = $rgsModel;

        return $this;
    }

    public function getRgsMark(): ?string
    {
        return $this->rgsMark;
    }

    public function setRgsMark(string $rgsMark): self
    {
        $this->rgsMark = $rgsMark;

        return $this;
    }

    public function getRgsCode(): ?string
    {
        return $this->rgsCode;
    }

    public function setRgsCode(string $rgsCode): self
    {
        $this->rgsCode = $rgsCode;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }


}

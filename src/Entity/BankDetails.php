<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BankDetails
 *
 * @ORM\Table(name="bank_details", indexes={@ORM\Index(name="IDX_5CB2632417EB4E41", columns={"agentId"})})
 * @ORM\Entity
 */
class BankDetails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="bank", type="string", length=255, nullable=false)
     */
    private $bank;

    /**
     * @var string
     *
     * @ORM\Column(name="bik", type="string", length=255, nullable=false)
     */
    private $bik;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255, nullable=false)
     */
    private $fio;

    /**
     * @var string
     *
     * @ORM\Column(name="additionalInfo", type="string", length=255, nullable=false)
     */
    private $additionalinfo;

    /**
     * @var string
     *
     * @ORM\Column(name="cardNumber", type="string", length=255, nullable=false)
     */
    private $cardnumber;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentId", referencedColumnName="id")
     * })
     */
    private $agentid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(string $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getBik(): ?string
    {
        return $this->bik;
    }

    public function setBik(string $bik): self
    {
        $this->bik = $bik;

        return $this;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getAdditionalinfo(): ?string
    {
        return $this->additionalinfo;
    }

    public function setAdditionalinfo(string $additionalinfo): self
    {
        $this->additionalinfo = $additionalinfo;

        return $this;
    }

    public function getCardnumber(): ?string
    {
        return $this->cardnumber;
    }

    public function setCardnumber(string $cardnumber): self
    {
        $this->cardnumber = $cardnumber;

        return $this;
    }

    public function getAgentid(): ?Agents
    {
        return $this->agentid;
    }

    public function setAgentid(?Agents $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }


}

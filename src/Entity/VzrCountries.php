<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VzrCountries
 *
 * @ORM\Table(name="vzr_countries")
 * @ORM\Entity
 */
class VzrCountries
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="liberty_id", type="string", length=50, nullable=false)
     */
    private $libertyId;

    /**
     * @var string
     *
     * @ORM\Column(name="ingos_id", type="string", length=50, nullable=false)
     */
    private $ingosId;

    /**
     * @var string
     *
     * @ORM\Column(name="ingos_currency", type="string", length=3, nullable=false, options={"fixed"=true})
     */
    private $ingosCurrency;

    /**
     * @var int
     *
     * @ORM\Column(name="ingos_minLim", type="integer", nullable=false)
     */
    private $ingosMinlim = '0';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLibertyId(): ?string
    {
        return $this->libertyId;
    }

    public function setLibertyId(string $libertyId): self
    {
        $this->libertyId = $libertyId;

        return $this;
    }

    public function getIngosId(): ?string
    {
        return $this->ingosId;
    }

    public function setIngosId(string $ingosId): self
    {
        $this->ingosId = $ingosId;

        return $this;
    }

    public function getIngosCurrency(): ?string
    {
        return $this->ingosCurrency;
    }

    public function setIngosCurrency(string $ingosCurrency): self
    {
        $this->ingosCurrency = $ingosCurrency;

        return $this;
    }

    public function getIngosMinlim(): ?int
    {
        return $this->ingosMinlim;
    }

    public function setIngosMinlim(int $ingosMinlim): self
    {
        $this->ingosMinlim = $ingosMinlim;

        return $this;
    }


}

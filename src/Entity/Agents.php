<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agents
 *
 * @ORM\Table(name="agents", uniqueConstraints={@ORM\UniqueConstraint(name="agents_email", columns={"email"}), @ORM\UniqueConstraint(name="agents_inn", columns={"inn"}), @ORM\UniqueConstraint(name="agents_login", columns={"login"}), @ORM\UniqueConstraint(name="agents_phone", columns={"phone"}), @ORM\UniqueConstraint(name="agents_resetToken", columns={"resetToken"}), @ORM\UniqueConstraint(name="agents_contractNumber", columns={"contractNumber"})})
 * @ORM\Entity
 */
class Agents
{
    /**
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="agenttoid")
     * @Assert\Valid()
     */
    protected $invoices;

    public function __construct()
    {
        $this->invoices = new ArrayCollection();
    }
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=255, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    /**
     * @var int
     *
     * @ORM\Column(name="parentId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $parentid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="defaultBankDetailsId", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $defaultbankdetailsid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=0, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="cityKladr", type="string", length=255, nullable=false)
     */
    private $citykladr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="resetToken", type="string", length=255, nullable=true)
     */
    private $resettoken;

    /**
     * @var string|null
     *
     * @ORM\Column(name="details", type="text", length=0, nullable=true)
     */
    private $details;

    /**
     * @var string|null
     *
     * @ORM\Column(name="inn", type="string", length=255, nullable=true)
     */
    private $inn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="credentialsType", type="smallint", nullable=true)
     */
    private $credentialstype;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isAchieved", type="smallint", nullable=true)
     */
    private $isachieved;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="lastPaymentDate", type="datetime", nullable=true)
     */
    private $lastpaymentdate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="contractNumber", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $contractnumber;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="contractDate", type="date", nullable=true)
     */
    private $contractdate;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getParentid(): ?int
    {
        return $this->parentid;
    }

    public function setParentid(int $parentid): self
    {
        $this->parentid = $parentid;

        return $this;
    }

    public function getDefaultbankdetailsid(): ?int
    {
        return $this->defaultbankdetailsid;
    }

    public function setDefaultbankdetailsid(?int $defaultbankdetailsid): self
    {
        $this->defaultbankdetailsid = $defaultbankdetailsid;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCitykladr(): ?string
    {
        return $this->citykladr;
    }

    public function setCitykladr(string $citykladr): self
    {
        $this->citykladr = $citykladr;

        return $this;
    }

    public function getResettoken(): ?string
    {
        return $this->resettoken;
    }

    public function setResettoken(?string $resettoken): self
    {
        $this->resettoken = $resettoken;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(?string $inn): self
    {
        $this->inn = $inn;

        return $this;
    }

    public function getCredentialstype(): ?int
    {
        return $this->credentialstype;
    }

    public function setCredentialstype(?int $credentialstype): self
    {
        $this->credentialstype = $credentialstype;

        return $this;
    }

    public function getIsachieved(): ?int
    {
        return $this->isachieved;
    }

    public function setIsachieved(?int $isachieved): self
    {
        $this->isachieved = $isachieved;

        return $this;
    }

    public function getLastpaymentdate(): ?\DateTimeInterface
    {
        return $this->lastpaymentdate;
    }

    public function setLastpaymentdate(?\DateTimeInterface $lastpaymentdate): self
    {
        $this->lastpaymentdate = $lastpaymentdate;

        return $this;
    }

    public function getContractnumber(): ?int
    {
        return $this->contractnumber;
    }

    public function setContractnumber(?int $contractnumber): self
    {
        $this->contractnumber = $contractnumber;

        return $this;
    }

    public function getContractdate(): ?\DateTimeInterface
    {
        return $this->contractdate;
    }

    public function setContractdate(?\DateTimeInterface $contractdate): self
    {
        $this->contractdate = $contractdate;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requests
 *
 * @ORM\Table(name="requests", indexes={@ORM\Index(name="IDX_7B85D65117EB4E41", columns={"agentId"})})
 * @ORM\Entity
 */
class Requests
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="productTypeId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $producttypeid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="car", type="string", length=255, nullable=true)
     */
    private $car;

    /**
     * @var string
     *
     * @ORM\Column(name="ident", type="string", length=255, nullable=false)
     */
    private $ident;

    /**
     * @var string
     *
     * @ORM\Column(name="fio", type="string", length=255, nullable=false)
     */
    private $fio;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="data", type="text", length=0, nullable=true)
     */
    private $data;

    /**
     * @var int|null
     *
     * @ORM\Column(name="insuranceCompanyId", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $insurancecompanyid;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string|null
     *
     * @ORM\Column(name="policy", type="text", length=0, nullable=true)
     */
    private $policy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="calculationId", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $calculationid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentId", referencedColumnName="id")
     * })
     */
    private $agentid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducttypeid(): ?int
    {
        return $this->producttypeid;
    }

    public function setProducttypeid(int $producttypeid): self
    {
        $this->producttypeid = $producttypeid;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getCar(): ?string
    {
        return $this->car;
    }

    public function setCar(?string $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getIdent(): ?string
    {
        return $this->ident;
    }

    public function setIdent(string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getInsurancecompanyid(): ?int
    {
        return $this->insurancecompanyid;
    }

    public function setInsurancecompanyid(?int $insurancecompanyid): self
    {
        $this->insurancecompanyid = $insurancecompanyid;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getPolicy(): ?string
    {
        return $this->policy;
    }

    public function setPolicy(?string $policy): self
    {
        $this->policy = $policy;

        return $this;
    }

    public function getCalculationid(): ?int
    {
        return $this->calculationid;
    }

    public function setCalculationid(?int $calculationid): self
    {
        $this->calculationid = $calculationid;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAgentid(): ?Agents
    {
        return $this->agentid;
    }

    public function setAgentid(?Agents $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }


}

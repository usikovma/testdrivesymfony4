<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BannedEmails
 *
 * @ORM\Table(name="banned_emails", uniqueConstraints={@ORM\UniqueConstraint(name="banned_emails_email", columns={"email"})})
 * @ORM\Entity
 */
class BannedEmails
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


}

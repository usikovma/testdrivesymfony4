<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InsuranceRequests
 *
 * @ORM\Table(name="insurance_requests", indexes={@ORM\Index(name="IDX_DDD4A01331EC155F", columns={"calculationId"})})
 * @ORM\Entity
 */
class InsuranceRequests
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="requestIdentifier", type="string", length=255, nullable=false)
     */
    private $requestidentifier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="response", type="text", length=0, nullable=true)
     */
    private $response;

    /**
     * @var \Calculations
     *
     * @ORM\ManyToOne(targetEntity="Calculations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="calculationId", referencedColumnName="id")
     * })
     */
    private $calculationid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRequestidentifier(): ?string
    {
        return $this->requestidentifier;
    }

    public function setRequestidentifier(string $requestidentifier): self
    {
        $this->requestidentifier = $requestidentifier;

        return $this;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function setResponse(?string $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getCalculationid(): ?Calculations
    {
        return $this->calculationid;
    }

    public function setCalculationid(?Calculations $calculationid): self
    {
        $this->calculationid = $calculationid;

        return $this;
    }


}

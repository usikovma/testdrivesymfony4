<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice", indexes={@ORM\Index(name="IDX_90651744FC9D2669", columns={"agentToId"}), @ORM\Index(name="IDX_906517441069D1B2", columns={"agentFromId"})})
 * @ORM\Entity
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="productNumber", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $productnumber;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=false)
     */
    private $amount;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="tstamp", type="datetime", nullable=true)
     */
    private $tstamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFrom", type="date", nullable=false)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTo", type="date", nullable=false)
     */
    private $dateto;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fileName", type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint", nullable=false)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="rent", type="float", precision=10, scale=0, nullable=false)
     */
    private $rent = '0';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="paymentDate", type="datetime", nullable=true)
     */
    private $paymentdate;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentFromId", referencedColumnName="id")
     * })
     */
    private $agentfromid;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents", inversedBy="invoices")
     * @ORM\JoinColumn(name="agentToId", referencedColumnName="id", onDelete="CASCADE")
     */
    private $agenttoid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductnumber(): ?int
    {
        return $this->productnumber;
    }

    public function setProductnumber(int $productnumber): self
    {
        $this->productnumber = $productnumber;

        return $this;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTstamp(): ?\DateTimeInterface
    {
        return $this->tstamp;
    }

    public function setTstamp(?\DateTimeInterface $tstamp): self
    {
        $this->tstamp = $tstamp;

        return $this;
    }

    public function getDatefrom(): ?\DateTimeInterface
    {
        return $this->datefrom;
    }

    public function setDatefrom(\DateTimeInterface $datefrom): self
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    public function getDateto(): ?\DateTimeInterface
    {
        return $this->dateto;
    }

    public function setDateto(\DateTimeInterface $dateto): self
    {
        $this->dateto = $dateto;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRent(): ?float
    {
        return $this->rent;
    }

    public function setRent(float $rent): self
    {
        $this->rent = $rent;

        return $this;
    }

    public function getPaymentdate(): ?\DateTimeInterface
    {
        return $this->paymentdate;
    }

    public function setPaymentdate(?\DateTimeInterface $paymentdate): self
    {
        $this->paymentdate = $paymentdate;

        return $this;
    }

    public function getAgentfromid(): ?Agents
    {
        return $this->agentfromid;
    }

    public function setAgentfromid(?Agents $agentfromid): self
    {
        $this->agentfromid = $agentfromid;

        return $this;
    }

    public function getAgenttoid(): ?Agents
    {
        return $this->agenttoid;
    }

    public function setAgenttoid(?Agents $agenttoid): self
    {
        $this->agenttoid = $agenttoid;

        return $this;
    }


}

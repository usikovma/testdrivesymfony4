<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestMigrateRepository")
 */
class TestMigrate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filed;

    /**
     * @ORM\Column(type="smallint")
     */
    private $filedInt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFiled(): ?string
    {
        return $this->filed;
    }

    public function setFiled(string $filed): self
    {
        $this->filed = $filed;

        return $this;
    }

    public function getFiledInt(): ?int
    {
        return $this->filedInt;
    }

    public function setFiledInt(int $filedInt): self
    {
        $this->filedInt = $filedInt;

        return $this;
    }
}

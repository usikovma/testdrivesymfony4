<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rewards
 *
 * @ORM\Table(name="rewards", uniqueConstraints={@ORM\UniqueConstraint(name="rewards_agentId_productId_idx", columns={"agentId", "productId"})}, indexes={@ORM\Index(name="IDX_E9221E3717EB4E41", columns={"agentId"})})
 * @ORM\Entity
 */
class Rewards
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="productId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $productid;

    /**
     * @var float
     *
     * @ORM\Column(name="reward", type="float", precision=10, scale=0, nullable=false)
     */
    private $reward;

    /**
     * @var \Agents
     *
     * @ORM\ManyToOne(targetEntity="Agents")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agentId", referencedColumnName="id")
     * })
     */
    private $agentid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function setProductid(int $productid): self
    {
        $this->productid = $productid;

        return $this;
    }

    public function getReward(): ?float
    {
        return $this->reward;
    }

    public function setReward(float $reward): self
    {
        $this->reward = $reward;

        return $this;
    }

    public function getAgentid(): ?Agents
    {
        return $this->agentid;
    }

    public function setAgentid(?Agents $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }


}

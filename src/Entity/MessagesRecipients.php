<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MessagesRecipients
 *
 * @ORM\Table(name="messages_recipients", indexes={@ORM\Index(name="IDX_A690A3CA4C3A0DA", columns={"messageId"})})
 * @ORM\Entity
 */
class MessagesRecipients
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="agentId", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $agentid;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status;

    /**
     * @var \Messages
     *
     * @ORM\ManyToOne(targetEntity="Messages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="messageId", referencedColumnName="id")
     * })
     */
    private $messageid;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgentid(): ?int
    {
        return $this->agentid;
    }

    public function setAgentid(int $agentid): self
    {
        $this->agentid = $agentid;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMessageid(): ?Messages
    {
        return $this->messageid;
    }

    public function setMessageid(?Messages $messageid): self
    {
        $this->messageid = $messageid;

        return $this;
    }


}

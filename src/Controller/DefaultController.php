<?php


namespace App\Controller;


use App\Entity\Agents;
use App\Entity\Invoice;
use App\Repository\AgentsRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    protected $container;
    private $serializer;

    private $router;

    public function __construct(RouterInterface $router, SerializerInterface $serializer)
    {
        $this->router = $router;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/login")
     */
    public function login()
    {
        return new Response('<html><body>login page!</body></html>');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {

    }

    /**
     * @Route("/admin")
     */
    public function admin()
    {
        return new Response('<html><body>Admin page!</body></html>');
    }

    /**
     * @Route("/", name="index")
     */

    public function index()
    {
        return $this->render('auth/login.html.twig');
    }

    /**
     * @Route("/test/{id}", name="test")
     */
    public function test(Agents $agent1,
                         EntityManagerInterface $entityManager,
                         AgentsRepository $agentsRepository,
                         Connection $connection)
    {

        $invoice = $entityManager->getRepository(Invoice::class)
            ->findOneBy(['agenttoid' => 106]);

        $criteria = new \stdClass();
        $criteria->id = 106;
        $criteria->branchingRequired = true;

        $agent2 = $agentsRepository->findAgent($criteria);
        $agentDql = '';//$agentsRepository->findAgentDQL($criteria);
        $agent = $agentsRepository->findAgentWithInvoice($criteria)[0];

        $prices = $connection->fetchAll('SELECT * FROM prices');

        return $this->render('index.html.twig', ['agent1' => $agent1,
            'invoice' => $invoice,
            'agent' => $agent,
            'agentDql' => $agentDql,
            'prices' => $prices]);
    }
}
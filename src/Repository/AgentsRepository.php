<?php


namespace App\Repository;

use App\Entity\Agents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class AgentsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Agents::class);
    }

    public function findAgentWithInvoice($criteria)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('inv')
            ->leftJoin('a.invoices', 'inv')
            ->where('a.id = :id')
            ->setParameter('id', $criteria->id)
            ->orderBy('a.name', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    public function findAgent($criteria)
    {
        $qb = $this->createQueryBuilder('a');

        if($criteria->branchingRequired){
           $qb->addSelect();
        }

        return '';
    }

}
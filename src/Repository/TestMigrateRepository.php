<?php

namespace App\Repository;

use App\Entity\TestMigrate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TestMigrate|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestMigrate|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestMigrate[]    findAll()
 * @method TestMigrate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestMigrateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TestMigrate::class);
    }

    // /**
    //  * @return TestMigrate[] Returns an array of TestMigrate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TestMigrate
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

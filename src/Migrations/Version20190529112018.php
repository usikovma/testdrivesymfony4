<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190529112018 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE test_migrate (id INT AUTO_INCREMENT NOT NULL, filed VARCHAR(255) NOT NULL, filed_int SMALLINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE event');
        $this->addSql('ALTER TABLE bank_details DROP FOREIGN KEY bank_details_fk_Agent');
        $this->addSql('ALTER TABLE bank_details CHANGE agentId agentId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE bank_details ADD CONSTRAINT FK_5CB2632417EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE calculations DROP FOREIGN KEY calculations_fk_Request');
        $this->addSql('ALTER TABLE calculations CHANGE requestId requestId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE calculations ADD CONSTRAINT FK_4BFD195EA1637001 FOREIGN KEY (requestId) REFERENCES requests (id)');
        $this->addSql('ALTER TABLE drafts DROP FOREIGN KEY drafts_fk_Agent');
        $this->addSql('ALTER TABLE drafts CHANGE agentId agentId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE drafts ADD CONSTRAINT FK_EC2AE4C017EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE insurance_requests DROP FOREIGN KEY insurance_requests_fk_Calculation');
        $this->addSql('ALTER TABLE insurance_requests CHANGE calculationId calculationId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE insurance_requests ADD CONSTRAINT FK_DDD4A01331EC155F FOREIGN KEY (calculationId) REFERENCES calculations (id)');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY invoice_fk_Agent1');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY invoice_fk_Agent2');
        $this->addSql('ALTER TABLE invoice DROP isViewed, CHANGE agentFromId agentFromId INT UNSIGNED DEFAULT NULL, CHANGE agentToId agentToId INT UNSIGNED DEFAULT NULL, CHANGE rent rent DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_906517441069D1B2 FOREIGN KEY (agentFromId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT FK_90651744FC9D2669 FOREIGN KEY (agentToId) REFERENCES agents (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY messages_fk_agent');
        $this->addSql('ALTER TABLE messages CHANGE agentId agentId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E9617EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE messages_recipients DROP FOREIGN KEY messages_recipients_fk_message');
        $this->addSql('ALTER TABLE messages_recipients CHANGE messageId messageId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE messages_recipients ADD CONSTRAINT FK_A690A3CA4C3A0DA FOREIGN KEY (messageId) REFERENCES messages (id)');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY prices_fk_Agent');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY prices_fk_Request');
        $this->addSql('ALTER TABLE prices CHANGE agentId agentId INT UNSIGNED DEFAULT NULL, CHANGE requestId requestId INT UNSIGNED DEFAULT NULL, CHANGE invoiceId invoiceId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D5917EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT FK_E4CB6D59A1637001 FOREIGN KEY (requestId) REFERENCES requests (id)');
        $this->addSql('ALTER TABLE requests DROP FOREIGN KEY requests_fk_agent');
        $this->addSql('ALTER TABLE requests CHANGE agentId agentId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE requests ADD CONSTRAINT FK_7B85D65117EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE rewards DROP FOREIGN KEY rewards_fk_Agent');
        $this->addSql('ALTER TABLE rewards CHANGE agentId agentId INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE rewards ADD CONSTRAINT FK_E9221E3717EB4E41 FOREIGN KEY (agentId) REFERENCES agents (id)');
        $this->addSql('ALTER TABLE ts_osago DROP tinkoff_model_id, DROP max_model_id, DROP max_model, DROP max_mark, DROP max_mark_id, DROP max_type_id, CHANGE ingos_isn ingos_isn VARCHAR(20) NOT NULL, CHANGE ingos_model ingos_model VARCHAR(100) NOT NULL, CHANGE ingos_mark ingos_mark VARCHAR(100) NOT NULL, CHANGE alpha_model alpha_model VARCHAR(100) NOT NULL, CHANGE alpha_mark alpha_mark VARCHAR(100) NOT NULL, CHANGE sib_model_id sib_model_id VARCHAR(10) NOT NULL, CHANGE sib_model_name sib_model_name VARCHAR(100) NOT NULL, CHANGE sib_mark_id sib_mark_id VARCHAR(10) NOT NULL, CHANGE ren_model ren_model VARCHAR(100) NOT NULL, CHANGE ren_mark ren_mark VARCHAR(100) NOT NULL, CHANGE rgs_model rgs_model VARCHAR(100) NOT NULL, CHANGE rgs_mark rgs_mark VARCHAR(100) NOT NULL, CHANGE rgs_code rgs_code VARCHAR(100) NOT NULL, CHANGE type type VARCHAR(100) NOT NULL, CHANGE category category VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE vzr_countries CHANGE ingos_minLim ingos_minLim INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE event (id INT UNSIGNED AUTO_INCREMENT NOT NULL, description VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, isEnabled INT UNSIGNED NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE test_migrate');
        $this->addSql('ALTER TABLE bank_details DROP FOREIGN KEY FK_5CB2632417EB4E41');
        $this->addSql('ALTER TABLE bank_details CHANGE agentId agentId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE bank_details ADD CONSTRAINT bank_details_fk_Agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE calculations DROP FOREIGN KEY FK_4BFD195EA1637001');
        $this->addSql('ALTER TABLE calculations CHANGE requestId requestId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE calculations ADD CONSTRAINT calculations_fk_Request FOREIGN KEY (requestId) REFERENCES requests (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE drafts DROP FOREIGN KEY FK_EC2AE4C017EB4E41');
        $this->addSql('ALTER TABLE drafts CHANGE agentId agentId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE drafts ADD CONSTRAINT drafts_fk_Agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE insurance_requests DROP FOREIGN KEY FK_DDD4A01331EC155F');
        $this->addSql('ALTER TABLE insurance_requests CHANGE calculationId calculationId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE insurance_requests ADD CONSTRAINT insurance_requests_fk_Calculation FOREIGN KEY (calculationId) REFERENCES calculations (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_906517441069D1B2');
        $this->addSql('ALTER TABLE invoice DROP FOREIGN KEY FK_90651744FC9D2669');
        $this->addSql('ALTER TABLE invoice ADD isViewed INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE rent rent DOUBLE PRECISION DEFAULT \'0\' NOT NULL, CHANGE agentFromId agentFromId INT UNSIGNED NOT NULL, CHANGE agentToId agentToId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT invoice_fk_Agent1 FOREIGN KEY (agentFromId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE invoice ADD CONSTRAINT invoice_fk_Agent2 FOREIGN KEY (agentToId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E9617EB4E41');
        $this->addSql('ALTER TABLE messages CHANGE agentId agentId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT messages_fk_agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE messages_recipients DROP FOREIGN KEY FK_A690A3CA4C3A0DA');
        $this->addSql('ALTER TABLE messages_recipients CHANGE messageId messageId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE messages_recipients ADD CONSTRAINT messages_recipients_fk_message FOREIGN KEY (messageId) REFERENCES messages (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D5917EB4E41');
        $this->addSql('ALTER TABLE prices DROP FOREIGN KEY FK_E4CB6D59A1637001');
        $this->addSql('ALTER TABLE prices CHANGE invoiceId invoiceId INT UNSIGNED DEFAULT 0, CHANGE agentId agentId INT UNSIGNED NOT NULL, CHANGE requestId requestId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT prices_fk_Agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prices ADD CONSTRAINT prices_fk_Request FOREIGN KEY (requestId) REFERENCES requests (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE requests DROP FOREIGN KEY FK_7B85D65117EB4E41');
        $this->addSql('ALTER TABLE requests CHANGE agentId agentId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE requests ADD CONSTRAINT requests_fk_agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rewards DROP FOREIGN KEY FK_E9221E3717EB4E41');
        $this->addSql('ALTER TABLE rewards CHANGE agentId agentId INT UNSIGNED NOT NULL');
        $this->addSql('ALTER TABLE rewards ADD CONSTRAINT rewards_fk_Agent FOREIGN KEY (agentId) REFERENCES agents (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ts_osago ADD tinkoff_model_id VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, ADD max_model_id INT UNSIGNED NOT NULL, ADD max_model VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, ADD max_mark VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, ADD max_mark_id INT UNSIGNED NOT NULL, ADD max_type_id INT UNSIGNED NOT NULL, CHANGE ingos_isn ingos_isn VARCHAR(20) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE ingos_model ingos_model VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE ingos_mark ingos_mark VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE alpha_model alpha_model VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE alpha_mark alpha_mark VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE sib_model_id sib_model_id VARCHAR(10) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE sib_model_name sib_model_name VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE sib_mark_id sib_mark_id VARCHAR(10) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE ren_model ren_model VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE ren_mark ren_mark VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE rgs_model rgs_model VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE rgs_mark rgs_mark VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE rgs_code rgs_code VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE type type VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci, CHANGE category category VARCHAR(100) DEFAULT \'\' NOT NULL COLLATE utf8_general_ci');
        $this->addSql('ALTER TABLE vzr_countries CHANGE ingos_minLim ingos_minLim INT DEFAULT 0 NOT NULL');
    }
}
